<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public static $rules = [
        'title' => 'sometimes|string',
        'author' => 'sometimes|string',
        'publication_year' => 'sometimes|integer',
        'genre' => 'sometimes|string'
    ];
    protected $fillable = [
        'title',
        'author',
        'publication_year',
        'genre',
    ];
}