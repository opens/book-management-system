<?php

use App\Controllers\BookController;

$app->get('/api/books', [BookController::class, 'index']);
$app->get('/api/books/{id}', [BookController::class, 'show']);
$app->post('/api/books', [BookController::class, 'store']);
$app->put('/api/books/{id}', [BookController::class, 'update']);
$app->delete('/api/books/{id}', [BookController::class, 'destroy']);