<?php

namespace App\Validation;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Translation\FileLoader;
use Illuminate\Translation\Translator;
use Illuminate\Validation\Factory;

class Validation
{
    private static $factory;
    private static $messages = [
        'required' => 'The :attribute field is required.',
        'string' => 'The :attribute field must be a string.',
        'integer' => 'The :attribute field must be an integer.',
    ];

    public static function make($data, $rules)
    {
        if (self::$factory === null) {
            $filesystem = new Filesystem();
            $fileLoader = new FileLoader($filesystem, '');
            $translator = new Translator($fileLoader, 'en');
            self::$factory = new Factory($translator);
        }

        return self::$factory->make($data, $rules, self::$messages);
    }
}