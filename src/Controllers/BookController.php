<?php

namespace App\Controllers;

use App\Models\Book;
use App\Validation\Validation;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class BookController
{
    public function index(Request $request, Response $response)
    {
        $books = Book::all();
        $response->getBody()->write(json_encode(['data' => $books->toArray()]));
        return $response;
    }

    public function show(Request $request, Response $response, array $args)
    {
        $book = Book::find((int)$args['id']);

        if (!$book) {
            $response->withStatus(404)->getBody()->write(json_encode(['message' => 'Book not found']));
        } else {
            $response->getBody()->write(json_encode([
                'data' => $book->toArray()
            ]));
        }

        return $response;
    }

    public function store(Request $request, Response $response)
    {
        $validator = Validation::make($request->getParsedBody(), Book::$rules);

        if ($validator->fails()) {
            $response->withStatus(422)->getBody()->write(json_encode(['message' => $validator->errors()->first()]));
            return $response;
        }

        $book = new Book($validator->validate());

        if ($book->save()) {
            $response->getBody()->write(json_encode([
                'message' => 'Book created successfully'
            ]));
        } else {
            $response->withStatus(201)->getBody()->write(json_encode([
                'message' => 'Failed to create the book'
            ]));
        }

        return $response;
    }

    public function update(Request $request, Response $response, array $args)
    {
        $validator = Validation::make($request->getParsedBody(), Book::$rules);

        if ($validator->fails()) {
            $response->withStatus(422)->getBody()->write(json_encode(['message' => $validator->errors()->first()]));
            return $response;
        }

        $book = Book::find((int)$args['id']);
        if (!$book) {
            $response->withStatus(404)->getBody()->write(json_encode(['message' => 'Book not found']));
            return $response;
        }
        $book->fill($validator->validate());

        if ($book->save()) {
            $response->getBody()->write(json_encode([
                'message' => 'Book updated successfully'
            ]));
        } else {
            $response->withStatus(422)->getBody()->write(json_encode([
                'message' => 'Failed to update the book'
            ]));
        }

        return $response;
    }

    public function destroy(Request $request, Response $response, array $args)
    {
        $book = Book::find((int)$args['id']);

        if (!$book) {
            $response->withStatus(404)->getBody()->write(json_encode(['message' => 'Book not found']));
            return $response;
        }

        if ($book->delete()) {
            $response->getBody()->write(json_encode(['message' => 'Book deleted']));
        } else {
            $response->withStatus(422)->getBody()->write(json_encode(['message' => 'Failed to delete the book']));
        }

        return $response;
    }
}