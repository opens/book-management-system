CREATE TABLE `books`
(
    `id`               int          NOT NULL,
    `title`            varchar(255) NOT NULL,
    `author`           varchar(20)  NOT NULL,
    `publication_year` int          NOT NULL,
    `genre`            varchar(255) NOT NULL,
    `created_at`       timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at`       timestamp    NULL     DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;

ALTER TABLE `books`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `books`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;
COMMIT;

INSERT INTO slim.books (id, title, author, publication_year, genre, created_at, updated_at)
VALUES (2, 'new title', 'new author', 2023, 'new genre', '2023-07-10 20:42:55', '2023-07-10 21:23:40');
INSERT INTO slim.books (id, title, author, publication_year, genre, created_at, updated_at)
VALUES (3, 'title12', 'author34', 2023, 'genre56', '2023-07-10 21:19:05', '2023-07-10 21:19:05');
INSERT INTO slim.books (id, title, author, publication_year, genre, created_at, updated_at)
VALUES (4, 'title12', 'author34', 2022, 'genre56', '2023-07-10 21:20:27', '2023-07-10 21:20:27');
INSERT INTO slim.books (id, title, author, publication_year, genre, created_at, updated_at)
VALUES (5, 'title12 ', 'author', 2021, 'genre ', '2023-07-10 21:23:32', '2023-07-10 21:23:32');
